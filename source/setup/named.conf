#
# Insert these snippets into your named.conf or bind.conf to configure
# the BIND nameserver.
#

# If you have a very recent BIND, supporting GSS-TSIG, 
# insert this into options {}  (otherwise omit, it is not required if we don't accept updates)
tkey-gssapi-credential "DNS/${DNSDOMAIN}";
tkey-domain "${REALM}";

# You should always include the actual zone configuration reference:
zone "${DNSDOMAIN}." IN {
        type master;
        file "${DNSDOMAIN}.zone";
	update-policy {
		/* use ANY only for Domain controllers for now */
		/* for normal machines A AAAA PTR is probbaly all is needed */
		grant ${HOSTNAME}.${DNSDOMAIN}@${REALM} name ${HOSTNAME}.${DNSDOMAIN} ANY;
	};
};

# Also, you need to change your init scripts to set this environment variable
# for named: KRB5_KTNAME so that it points to the keytab generated.
# In RedHat derived systems such RHEL/CentOS/Fedora you can add the following
# line to the /etc/sysconfig/named file:
# export KRB5_KTNAME=${DNS_KEYTAB_ABS}
#
# Please note that most distributions have BIND configured to run under
# a non-root user account.  For example, Fedora Core 6 (FC6) runs BIND as
# the user "named" once the daemon relinquishes its rights.  Therefore,
# the file "${DNS_KEYTAB}" must be readable by the user that BIND run as.
# If BIND is running as a non-root user, the "${DNS_KEYTAB}" file must have its
# permissions altered to allow the daemon to read it.  In the FC6
# example, execute the commands:
# 
# chgrp named ${DNS_KEYTAB_ABS}
# chmod g+r ${DNS_KEYTAB_ABS}
