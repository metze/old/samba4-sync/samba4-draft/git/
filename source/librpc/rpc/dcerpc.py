# This file was automatically generated by SWIG (http://www.swig.org).
# Version 1.3.33
#
# Don't modify this file, modify the SWIG interface instead.

import _dcerpc
import new
new_instancemethod = new.instancemethod
try:
    _swig_property = property
except NameError:
    pass # Python < 2.2 doesn't have 'property'.
def _swig_setattr_nondynamic(self,class_type,name,value,static=1):
    if (name == "thisown"): return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'PySwigObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    if (not static) or hasattr(self,name):
        self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)

def _swig_setattr(self,class_type,name,value):
    return _swig_setattr_nondynamic(self,class_type,name,value,0)

def _swig_getattr(self,class_type,name):
    if (name == "thisown"): return self.this.own()
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError,name

def _swig_repr(self):
    try: strthis = "proxy of " + self.this.__repr__()
    except: strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

import types
try:
    _object = types.ObjectType
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0
del types


def _swig_setattr_nondynamic_method(set):
    def set_attr(self,name,value):
        if (name == "thisown"): return self.this.own(value)
        if hasattr(self,name) or (name == "this"):
            set(self,name,value)
        else:
            raise AttributeError("You cannot add attributes to %s" % self)
    return set_attr


import param
class Credentials(object):
    thisown = _swig_property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc='The membership flag')
    __repr__ = _swig_repr
    def __init__(self, *args, **kwargs): 
        _dcerpc.Credentials_swiginit(self,_dcerpc.new_Credentials(*args, **kwargs))
    __swig_destroy__ = _dcerpc.delete_Credentials
Credentials.get_username = new_instancemethod(_dcerpc.Credentials_get_username,None,Credentials)
Credentials.set_username = new_instancemethod(_dcerpc.Credentials_set_username,None,Credentials)
Credentials.get_password = new_instancemethod(_dcerpc.Credentials_get_password,None,Credentials)
Credentials.set_password = new_instancemethod(_dcerpc.Credentials_set_password,None,Credentials)
Credentials.get_domain = new_instancemethod(_dcerpc.Credentials_get_domain,None,Credentials)
Credentials.set_domain = new_instancemethod(_dcerpc.Credentials_set_domain,None,Credentials)
Credentials.get_realm = new_instancemethod(_dcerpc.Credentials_get_realm,None,Credentials)
Credentials.set_realm = new_instancemethod(_dcerpc.Credentials_set_realm,None,Credentials)
Credentials.parse_string = new_instancemethod(_dcerpc.Credentials_parse_string,None,Credentials)
Credentials.get_bind_dn = new_instancemethod(_dcerpc.Credentials_get_bind_dn,None,Credentials)
Credentials.set_bind_dn = new_instancemethod(_dcerpc.Credentials_set_bind_dn,None,Credentials)
Credentials.get_workstation = new_instancemethod(_dcerpc.Credentials_get_workstation,None,Credentials)
Credentials.set_workstation = new_instancemethod(_dcerpc.Credentials_set_workstation,None,Credentials)
Credentials.guess = new_instancemethod(_dcerpc.Credentials_guess,None,Credentials)
Credentials.is_anonymous = new_instancemethod(_dcerpc.Credentials_is_anonymous,None,Credentials)
Credentials.get_nt_hash = new_instancemethod(_dcerpc.Credentials_get_nt_hash,None,Credentials)
Credentials.authentication_requested = new_instancemethod(_dcerpc.Credentials_authentication_requested,None,Credentials)
Credentials.wrong_password = new_instancemethod(_dcerpc.Credentials_wrong_password,None,Credentials)
Credentials_swigregister = _dcerpc.Credentials_swigregister
Credentials_swigregister(Credentials)

pipe_connect = _dcerpc.pipe_connect
dcerpc_server_name = _dcerpc.dcerpc_server_name


